import "./App.css";
import styled from "styled-components";
import { Board, Winner } from "./Board";
import { StartScreen } from "./StartScreen";
import React, { useState } from "react";
import { ResetScreen } from "./ResetScreen";
import { motion } from "framer-motion";

const variants = {
  game: {
    opacity: 1,
    scale: 1,
    width: "100px",
    height: "50px",
    transition: { type: "spring", duration: 0.8 },
  },
  start: {
    opacity: 1,
    scale: 1,
    width: "100px",
    height: "100px",
    transition: {
      type: "spring",
      duration: 0.8,
    },
  },
  reset: {
    opacity: 1,
    scale: 1,
    width: "50px",
    height: "25px",
    transition: { type: "spring", duration: 0.8 },
  },
  hidden: { opacity: 0, scale: 0.8 },
};

export const BoardContainer = styled(motion.div).attrs(() => ({
  initial: "hidden",
  variants,
}))`
  background: #ffffff;
  border-radius: 5px;
  box-shadow: -6px 10px 30px 4px #00000033;
  border: 10px solid white;
  display: flex;
  justify-content: center;
  align-items: center;
`;

type GameState = "start" | "game" | "reset";

const App = () => {
  const [winner, setWinner] = useState<Winner>();
  const [gameState, setGameState] = useState<GameState>("start");

  const onStart = () => {
    setGameState("game");
  };

  const onGameEnd = (winner: Winner) => {
    setWinner(winner);
    setGameState("reset");
  };

  const onReset = () => {
    setWinner(undefined);
    setGameState("game");
  };

  return (
    <div className="App">
      <h3>TIC TAC TOE</h3>
      <BoardContainer animate={"start"}>
        {
          {
            start: <StartScreen onStart={onStart} />,
            game: <Board onGameEnd={onGameEnd} />,
            reset: <ResetScreen winner={winner} onReset={onReset} />,
          }[gameState]
        }
      </BoardContainer>
    </div>
  );
};

export default App;
