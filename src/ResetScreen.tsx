import { FC } from "react";
import { Winner } from "./Board";

type ResetScreenProps = {
  winner: Winner;
  onReset(): void;
};

export const ResetScreen: FC<ResetScreenProps> = ({ winner, onReset }) => {
  return (
    <>
      <h2>{winner !== "tie" ? `${winner} is a winner` : `its a tie`} </h2>
      <button onClick={onReset}>Restart</button>
    </>
  );
};
