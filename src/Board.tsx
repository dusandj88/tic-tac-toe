import { FC, useEffect, useState } from "react";
import { Cell, CellValue } from "./Cell";
import styled from "styled-components";

export const BoardWrapper = styled.div`
  background-color: #999999;
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: auto auto auto;
  grid-template-rows: auto auto auto;
  row-gap: 1px;
  column-gap: 1px;
`;

export type Winner = CellValue | "tie";

type BoardProps = {
  onGameEnd(winner: Winner): void;
};

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

export const Board: FC<BoardProps> = ({ onGameEnd }) => {
  const [cells, setCells] = useState<CellValue[]>(Array(9).fill(undefined));

  const currentShape: CellValue = cells.filter(Boolean).length % 2 ? "o" : "x";

  const winningCondition = winningConditions.find((condition) => {
    const line = condition.map((cellIndex) => cells[cellIndex]);
    return line[0] && line.every((cellValue) => cellValue === line[0]);
  });

  const tie = cells.filter(Boolean).length === 9;

  const winningShape = winningCondition
    ? cells[winningCondition[0]]
    : undefined;

  useEffect(() => {
    if (winningShape) {
      return onGameEnd(winningShape);
    }
    if (tie) {
      return onGameEnd("tie");
    }
  }, [winningShape, tie, onGameEnd]);

  const toggleCell = (index: number) => {
    setCells((cells) =>
      cells.map((cell, cellIndex) =>
        index === cellIndex && cell === undefined ? currentShape : cell
      )
    );
  };

  return (
    <BoardWrapper>
      {cells.map((cell, index) => (
        <Cell key={index} value={cell} toggle={toggleCell} index={index} />
      ))}
    </BoardWrapper>
  );
};
